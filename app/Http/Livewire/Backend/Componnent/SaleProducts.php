<?php

namespace App\Http\Livewire\Backend\Componnent;

use Livewire\Component;
use App\Models\Customer;
use App\Models\Product_type;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use App\Models\Sales as ModelsSales;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Models\Products as ModelsProducts;
use App\Models\Sale_detail as ModelsSaleItem;

class SaleProducts extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $no1 = 1;
    public $search;
    public $cartData;
    public $cartCount;
    public $cartTotal;
    public $customerSelect;
    public $product_type_id;
    public $customer_id,$customer_data;
    public $mode = "cod";
    public function mount()
    {
        $this->customerSelect = Customer::all();
    }
    public function render()
    {
        $customers = Customer::all();
        if (!empty($this->customer)) {
            $this->customer_data = Customer::orderBy('id', 'desc')
                ->where('id', $this->customer)->first();
        }
            // $products = ModelsProducts::where('name', 'like', '%' . $this->search . '%')->paginate(10);
            $product_type = Product_type::orderBy('id','desc')->get();
            if(!empty($this->product_type_id)){
                if(!empty($this->search)){
                    $products = ModelsProducts::orderBy('id','desc')
                    ->where('product_type_id', $this->product_type_id)
                    ->where(function($query){
                        $query->where('code', 'like', '%' . $this->search . '%')
                        ->orWhere('name', 'like', '%' . $this->search . '%');
                    })->paginate(10);
                }else{
                    $products = ModelsProducts::orderBy('id','desc')
                    ->where('product_type_id', $this->product_type_id)
                    ->paginate(10);
                }
            }else{
                if(!empty($this->search)){
                    $products = ModelsProducts::orderBy('id','desc')
                    ->where(function($query){
                        $query->where('code', 'like', '%' . $this->search . '%')
                        ->orWhere('name', 'like', '%' . $this->search . '%');
                    })->paginate(10);
                }else{
                    $products = ModelsProducts::paginate(10);
                }
            }
        $this->cartData = Cart::content();
        $this->cartCount = Cart::count();
        $this->cartTotal = Cart::subTotal();
        return view('livewire.backend.componnent.sale-products', compact('products','product_type'))->layout('layouts.backend.base');
    }

    // add to cart
    public function addCart($p_id)
    {
        $products = ModelsProducts::findOrFail($p_id);
        $itemsId = $products->id;
        $itemsName = $products->name;
        $itemsQty = 1;
        $itemsPrice = (float)$products->sell_price;
        Cart::add($itemsId, $itemsName, $itemsQty, $itemsPrice);
        $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມໃສ່ກະຕ່າເເລ້ວ!']);
    }
    // update cart
    public $rowId, $qty;
    public function updateCart($qty, $rowId, $status)
    {
        if ($status == 1) {
            $this->rowId = $rowId;
            $this->qty = $qty + 1;
            Cart::update($this->rowId, $this->qty);
            $this->emit('alert', ['type' => 'success', 'message' => 'ເພີ່ມຂື້ນ!']);
            return;
        } else {
            $this->rowId = $rowId;
            $this->qty = $qty - 1;
            Cart::update($this->rowId, $this->qty);
            $this->emit('alert', ['type' => 'error', 'message' => 'ລຶບອອກ!']);
        }
    }
    // remove cart
    public function removeCart()
    {
        Cart::destroy();
        $this->emit('alert', ['type' => 'success', 'message' => 'ລຶບລ້າງກະຕ່າ!']);
    }
    // show cart with modal
    public function _checkout()
    {
        if ($this->cartCount == 0) {
            $this->emit('alert', ['type' => 'error', 'message' => 'ກະຕ່າວ່າງ!']);
            return;
        } else {
            $this->showaddform();
        }
    }
    // sale save
    public $customer, $cmname, $cmlastname, $cmphone;
    public function _sale()
    {
        // $this->validate([
        //     'customer' => 'required',
        // ]);
        DB::beginTransaction();
        // try {
            $sale = new ModelsSales();
            $sale->code = 'ST' . rand(100000, 999999);
            $sale->subtotal = str_replace(",", "", $this->cartTotal);
            $sale->total = str_replace(",", "", $this->cartTotal);
            $sale->status = 3;
            // $sale->payment = 0;
            $sale->mode = $this->mode;
            $sale->customer_id = 10;
            $sale->employee_id = auth()->user()->id;
            $sale->note = 'ຂາຍຫນ້າຮ້ານ';
            $sale->type_sales = 1;
            $sale->payment = 0;
            $sale->save();

            $cartData = Cart::content();
            foreach ($cartData as $item) {
                if (isset($item->id)) {
                    $saleItem = new ModelsSaleItem();
                    $saleItem->sales_id = $sale->id;
                    $saleItem->product_id = $item->id;
                    $saleItem->quantity = $item->qty;
                    $saleItem->save();
                    // update stock
                    $product = ModelsProducts::findOrFail($item->id);
                    $product->qty = $product->qty - $item->qty;
                    $product->save();
                    DB::commit();
                }
            }
            // check if cmname, cmlastname, cmphone is null to create new customer
            DB::commit();
            $this->closeaddform();
            Cart::destroy();
            $this->resetForm();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ຂາຍສິນຄ້າສຳເລັດ!',
                'icon'=>'success',
                'iconColor'=>'green',
            ]);
            // $this->emit('alert', ['type' => 'success', 'message' => 'ຂາຍສິນຄ້າສຳເລັດ!']);
            // $id = $sale->id;
            // return redirect()->route('sale-pos-print', ['id' => $id]);
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     $this->emit('alert', ['type' => 'error', 'message' => 'something wrong!']);
        //     $this->resetForm();
        // }
    }
    // reset supplier
    public function resetForm()
    {
        $this->customer = '';
    }

    // show form modal
    protected function showaddform()
    {
        $this->dispatchBrowserEvent('showforma');
    }
    protected function closeaddform()
    {
        $this->dispatchBrowserEvent('closeforma');
    }
}