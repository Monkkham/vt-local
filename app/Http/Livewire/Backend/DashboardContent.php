<?php

namespace App\Http\Livewire\Backend;

use App\Models\Sales;
use App\Models\Orders;
use Livewire\Component;
use App\Models\Customer;
use App\Models\Employee;
use App\Models\Products;
use App\Models\Supplier;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use App\Models\Sale_detail as ModelSaleItem;

class DashboardContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $totalCustomer, $totalEmployee, $totalProductItem, $totalOrderPending;
    public $userTop5, $employeeTop5, $productTop5, $lastOrderTop5;
    public $supplier_count,$orders_count,$sales_count,$order_sum,$sale_sum;
    public function render()
    {
        $this->totalCustomer = Customer::count();
        $this->totalEmployee = Employee::count();
        $this->totalProductItem = Products::count();
        $this->totalOrderPending = Orders::where('status', '1')->count();
        $this->userTop5 = Sales::select('customer_id', DB::raw('count(*) as total'))
            ->groupBy('customer_id')
            ->orderBy('total', 'DESC')
            ->limit(5)
            ->get();
        $this->employeeTop5 = Sales::select('employee_id', DB::raw('count(*) as total'))
            ->groupBy('employee_id')
            ->orderBy('total', 'DESC')
            ->limit(5)
            ->get();
        $this->productTop5 = ModelSaleItem::select('product_id', DB::raw('count(*) as total'))
            ->groupBy('product_id')
            ->orderBy('total', 'DESC')
            ->limit(5)
            ->get();
        $this->lastOrderTop5 = Sales::orderBy('id', 'DESC')->limit(5)->get();
        $sales = Sales::where('status',1)->paginate(5);
        $this->supplier_count = Supplier::count();
        $this->orders_count = Orders::where('status',2)->count();
        $this->sales_count = Sales::where('status',3)->count();
        $this->order_sum = Orders::where('status',2)->sum('total_money');
        $this->sale_sum = Sales::where('status',3)->sum('total');
        return view('livewire.backend.dashboard-content',compact('sales'))->layout('layouts.backend.base');
    }
}
