<?php

namespace App\Http\Livewire\Backend\Reports;

use App\Models\Sales;
use Livewire\Component;
use App\Models\NoteBook;

class IncomeReportContent extends Component
{
    public $start, $end;
    public $starts, $ends,$num_code;
    public function render()
    {
        $this->num_code = rand(10000000,99999999);
        $start = $this->starts;
        $end = date('Y-m-d', strtotime($this->ends . ' + ' . 1 . 'days'));
        $incomes = NoteBook::select('*')
            ->where('type',1)
            ->whereBetween('created_at', [$this->starts, $end])->get();
        $sum_money = NoteBook::select('money')
            ->where('type',1)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('money');
            $sum_sale = Sales::select('total')
            ->where('status',3)
            ->whereBetween('created_at', [$this->starts, $end])
            ->orderBy('id', 'desc')->sum('total');
        return view('livewire.backend.reports.income-report-content',compact('incomes','sum_money','sum_sale'))->layout('layouts.backend.base');
    }
    public function sub()
    {
       if(!empty($this->start || $this->end))
       {
        $this->starts = $this->start;
        $this->ends = $this->end;
       }else{
        $this->emit('alert', ['type' => 'error', 'message' => 'ກະລຸນາເລືອກວັນທີ່ຫາວັນທີ່ກ່ອນ!']);
       }
    }
}
