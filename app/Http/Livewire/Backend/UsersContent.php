<?php

namespace App\Http\Livewire\Backend;

use App\Models\Roles;
use Livewire\Component;
use App\Models\Employee;
use App\Models\Villages;
use App\Models\Districts;
use App\Models\Provinces;
use Livewire\WithPagination;
use Livewire\WithFileUploads;

class UsersContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $ID,$newimage,$search,$con_password;
    public 
    $roles_id,
    $village_id,
    $district_id,
    $province_id,
    $code,
    $image,
    $name,
    $name_la,
    $lastname,
    $password,
    $phone,
    $email,
    $address,
    $gender,
    $birthday,
    $status,
    $created_at,
    $updated_at;
    public function render()
    {
        $provinces = Provinces::get();
        if(!empty($this->province_id)){
            $this->districts = Districts::where('province_id',$this->province_id)->get();
        }
        if(!empty($this->district_id)){
            $this->villages = Villages::where('district_id',$this->district_id)->get();
        }
        $roles = Roles::all();
        $employee = Employee::orderBy('id','desc')
        ->where('name','like','%' . $this->search. '%')
        ->orwhere('phone','like','%' . $this->search. '%')
        ->paginate(10);
        return view('livewire.backend.users-content',compact('employee','provinces','roles'))->layout('layouts.backend.base');
    }
    public function resetform()
    {
           $this->roles_id=''; 
           $this->search='';
           $this->ID='';
    }
    public function showEdit($ids)
    {
        $data = Employee::find($ids);
        $this->ID=$data->id;
        $this->roles_id = $data->roles_id;
        $this->name = $data->name;
        $this->dispatchBrowserEvent('show-modal-roles');
    }

    public function update()
    {
        $this->validate([
            'roles_id'=>'required',
        ],[
            'roles_id.required'=>'ເລືອກ1ສິດກ່ອນ!',
        ]);
        $ids = $this->ID;
        $data = Employee::find($ids);
        $data->roles_id =$this->roles_id;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-roles');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ມອບສິດຫນ້າທີ່ສຳເລັດເເລ້ວ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        $this->resetform();
    }
}
