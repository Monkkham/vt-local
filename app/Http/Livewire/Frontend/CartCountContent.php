<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;

class CartCountContent extends Component
{
    protected $listeners=['refreshComponent'=>'$refresh'];
    public function render()
    {
        return view('livewire.frontend.cart-count-content');
    }
}
