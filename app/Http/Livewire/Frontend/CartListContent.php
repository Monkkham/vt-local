<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use Cart;
class CartListContent extends Component
{
    public $listeners=['refreshComponent'=>'$refresh'];
    public function render()
    {
        return view('livewire.frontend.cart-list-content');
    }
            //Remove product from list cart
            public function destroy($rowId)
            {
                $cart = Cart::instance('cart')->content()->where('rowId',$rowId);
                if($cart->isNotEmpty()){
                    Cart::instance('cart')->remove($rowId);
                }
               $this->dispatchBrowserEvent('swal', [
                'title' => 'ລຶບອອກກະຕ່າເເລ້ວ!',
                'icon'=>'success',
                'iconColor'=>'green',
                ]);
                $this->emitTo('frontend.cart-count-content','refreshComponent');
                $this->emitTo('frontend.cart-list-content','refreshComponent');
                $this->emitTo('frontend.checkout-content','refreshComponent');
            }
}
