<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;
use App\Models\Contact;
use App\Models\AboutCompany;

class ContactContent extends Component
{
    public $name,$phone,$email,$header,$note,$lat,$long,$latitude,$longitude;
    public function render()
    {
        $location = AboutCompany::get();
        return view('livewire.frontend.contact-content',compact('location'))->layout('layouts.front-end.base');
    }
    public function resetform()
    {
        $this->name = "";
        $this->phone = "";
        $this->email = "";
        $this->header = "";
        $this->note = "";
    }
    public function contact()
    {
        $this->validate([
            'name'=>'required',
            'phone'=>'required|unique:contact',
            'email'=>'required|unique:contact',
            'header'=>'required',
            'note'=>'required',
        ],[
            'name.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            'email.unique'=>'ຂໍ້ມູນນີ້ມີໃນລະບົບເເລ້ວ!',
            'email.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'header.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'note.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        $data = new Contact();
        $data->name = $this->name;
        $data->phone = $this->phone;
        $data->email = $this->email;
        $data->header = $this->header;
        $data->note = $this->note;
        $data->save();
        $this->resetform();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສົ່ງຂໍ້ມູນສຳເລັດເເລ້ວ!',
            'icon'=>'success',
            'iconColor'=>'green',
        ]);
        // // session()->flash('success', 'ສົ່ງຂໍ້ມູນສຳເລັດເເລ້ວ');
        // // return redirect(route('frontend.contact'));
    }
}
