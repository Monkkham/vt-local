<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Products;
use App\Models\Product_type;
use Livewire\Component;

class HeaderContent extends Component
{
    public $search, $product_type;
    public function mount()
    {
        $this->product_type = 'Allproducts';
        $this->fill(request()->only('search', 'product_type', 'product_type_id'));
    }
    public function render()
    {
        $products = Products::orderBy('id', 'desc')
            ->where('name', 'like', '%' . $this->search . '%')
            ->get();
        $producttypes = Product_type::all();
        return view('livewire.frontend.header-content',compact('products','producttypes'));
    }
}
