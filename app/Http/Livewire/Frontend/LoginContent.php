<?php

namespace App\Http\Livewire\Frontend;
use App\Models\Customer;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class LoginContent extends Component
{
    public $phone, $password, $remember;
    public function render()
    {
        return view('livewire.frontend.login-content')->layout('layouts.front-end.base');
    }
    public function login()
    {
        $this->validate([
            'phone'=>'required',
            'password'=>'required',
        ],[
            'phone.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ',
            'phone.min'=>'ເບີໂທຕ້ອງ 8 ຕົວເລກ',
            'password.required'=>'ປ້ອນຂໍ້ມູນກ່ອນ',
        ]);
        if (Auth::guard('web')->attempt([
            'phone' => $this->phone,
            'password' => $this->password],
              $this->remember)) 
            {
                $this->dispatchBrowserEvent('swal:login', [
                    'type' => 'success',  
                    'message' => 'ຍິນດີຕ້ອນຮັບ! (' . Auth::guard('web')->user()->name . ')',
                    // 'message' => 'ຍິນດີຕ້ອນຮັບ!', 
                    'text' => 'ເຂົ້າສູ່ລະບົບສຳເລັດເເລ້ວ'
                ]);
                return redirect(route('home'));
            }else{
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ເບີໂທ ຫລື ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ! ລອງໃຫມ່',
                    'icon'=>'error',
                    'iconColor'=>'warning',
                ]);
                // return redirect(route('frontend.login'));
            }
    }
    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect()->route('home');
    }
}
