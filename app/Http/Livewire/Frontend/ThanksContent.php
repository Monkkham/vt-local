<?php

namespace App\Http\Livewire\Frontend;

use Livewire\Component;

class ThanksContent extends Component
{
    public function render()
    {
        return view('livewire.frontend.thanks-content')->layout('layouts.front-end.base');
    }
}
