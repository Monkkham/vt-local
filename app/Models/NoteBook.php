<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoteBook extends Model
{
    use HasFactory;
    use HasFactory;
    protected $table = "notebook";
    protected $fillable = ['id','code','name','money','type','created_at','updated_at'];
}
