<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;
    protected $table = 'orders';
    protected $fillable =
    [
        'id',
        'employee_id',
        'supplier_id',
        'code',
        'total_money',
        'status', // 1 = ກວດສອບ, 2 = ນຳເຂົ້າສຳເລັດ,
        'mode', // cod = ເກັບເງິນປາຍທາງ, onepay = ເງິນໂອນ
        'payment', // 1 = ຍັງບໍ່ທັນຊຳລະ, 0 = ຊຳລະເເລ້ວ
        'onepay_image',
        'note',
        'created_at',
        'updated_at'
    ];
    public function order_detail()
    {
        return $this->belongsTo('App\Models\Order_detail', 'order_detail_id', 'id');
    }
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }
    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id', 'id');
    }
}