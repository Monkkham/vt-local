<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    use HasFactory;
    protected $table = "role_permissions";
    protected $fillable = [
        'role_id','permissions_id','created_at','updated_at'
    ];

    public function rolename()
    {
        return $this->belongsTo('App\Models\Roles','role_id','id');
    }

    public function permissionname()
    {
        return $this->belongsTo('App\Models\Permission','permissions_id','id');
    }
}
