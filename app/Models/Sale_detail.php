<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale_detail extends Model
{
    use HasFactory;
    protected $table = 'sale_detail';
    protected $fillable =
    [
        'id',
        'sales_id',
        'product_id',
        'quantity',
        'created_at',
        'updated_at'
    ];
    public function sales()
    {
        return $this->belongsTo('App\Models\Sales', 'sales_id', 'id');
    }
    public function products()
    {
        return $this->belongsTo('App\Models\Products', 'product_id', 'id');
    }
}