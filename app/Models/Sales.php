<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    use HasFactory;
    protected $table = 'sales';
    protected $fillable =
    [
        'id',
        'employee_id',
        'customer_id',
        'code',
        'subtotal',
        'discount',
        'tax',
        'total',
        'status', // 1 = ສັ່ງຊື້ໃຫມ່, 2 = ກຳລັງສົ່ງ, 3 = ສົ່ງສຳເລັດ 4 = ຖືກຍົກເລີກ
        'mode', // cod = ເກັບເງິນປາຍທາງ, onepay = ເງິນໂອນ
        'payment', // 1 = ຍັງບໍ່ທັນຊຳລະ, 0 = ຊຳລະເເລ້ວ
        'onepay_image',
        'type_sales', // type_sales 1 = ຂາຍຫນ້າຮ້ານ 2 = ຂາຍອອນລາຍ
        'note',
        'origin_start',
        'origin_end',
        'created_at',
        'updated_at'
    ];
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');
    }
}