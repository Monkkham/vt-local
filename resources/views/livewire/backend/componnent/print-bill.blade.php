<!-- Main content -->
<div wire:ignore.self class="modal fade" id="modal-print">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5 class="modal-title"> <button type="button" id="print"
                    class="btn btn-info btn-sm"><i class="fa fa-print"></i> ປິ່ຣນ
                </button></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body right_content">
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <small class="float-left">
                                    <p class="brand-link">
                                        <img style="width: 80px" src="{{ asset('logo/logo.jpg') }}" alt="AdminLTE Logo"
                                            class="brand-image img-circle elevation-3" style="opacity: .8"> ຮ້ານ: ນາງ Yee shop
                                    </p>
                                </small>
                                <small style="font-size: 16px" class="float-right text-sm"><b>ວັນທີ່:</b>
                                    {{ date('d-m-Y', strtotime($created_at)) }} <br> <b>ເວລາ:</b>
                                    {{ date('H:i:s', strtotime($created_at)) }}</small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            <address>
                                <strong><i class="fa fa-user"></i> ຜູ້ຈັດຊື້</strong><br>
                                @if (!empty($this->employee_data))
                                    <b>ຊື່:</b> {{ $this->employee_data->name }} {{ $this->employee_data->lastname }}
                                    <br>
                                    <b>ເບີໂທ:</b> {{ $this->employee_data->phone }} <br>
                                    <b>ອີເມວ:</b> {{ $this->employee_data->email }} <br>
                                    <b>ທີ່ຢູ່:</b>
                                    @if (!empty($this->supplier_data->province->name_la))
                                        {{ $this->employee_data->village->name_la }},
                                        {{ $this->employee_data->district->name_la }},
                                        {{ $this->supplier_data->province->name_la }} <br>
                                    @endif
                                @endif
                                {{-- <strong><i class="fas fa-home"></i> ສຳນັກງານໃຫຍ່</strong><br>
                                <b>ຕັ້ງຢູ່:</b> ບ້ານນາທ່າງອນ ເມືອງໄຊທານີ ນະຄອນຫລວງວຽງຈັນ<br>
                                <b>ໂທ/ແອັບ:</b> 020 55555555<br>
                                <b>Email:</b> info@gmail.com --}}
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            <address>
                                <strong><i class="fa fa-user"></i> ຜູ້ສະຫນອງ</strong><br>
                                @if (!empty($this->supplier_data))
                                    <b>ຊື່:</b> {{ $this->supplier_data->name }} {{ $this->supplier_data->lastname }}
                                    <br>
                                    <b>ເບີໂທ:</b> {{ $this->supplier_data->phone }} <br>
                                    <b>ອີເມວ:</b> {{ $this->supplier_data->email }} <br>
                                    <b>ທີ່ຢູ່:</b>
                                    @if (!empty($this->supplier_data->province->name_la))
                                        {{ $this->supplier_data->village->name_la }},
                                        {{ $this->supplier_data->district->name_la }},
                                        {{ $this->supplier_data->province->name_la }} <br>
                                    @endif

                                @endif
                            </address>
                        </div>
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead class="bg-light">
                                    <tr>
                                        <th colspan="6" class="text-center"><h5><u>ໃບບິນສັ່ງຊື້</u></h5></th>
                                    </tr>
                                    <tr>
                                        <th>ລຳດັບ</th>
                                        <th>ສິນຄ້າ</th>
                                        <th>ປະເພດ</th>
                                        <th>ລາຄາ</th>
                                        <th>ຈຳນວນ</th>
                                        <th>ເປັນເງິນ</th>
                                    </tr>
                                </thead>
                                @php
                                    $num = 1;
                                @endphp
                                <tbody>
                                    @foreach ($this->orderdetail_data as $item)
                                        @if (!empty($item->products))
                                            <tr>
                                                <td>{{ $num++ }}</td>
                                                <td class="text-bold">{{ $item->products->name }}</td>
                                                <td>
                                                    @if(!empty($item->products->product_type))
                                                    {{ $item->products->product_type->name }}
                                                    @endif
                                                </td>
                                                <td>{{ number_format($item->products->sell_price) }} ₭</td>
                                                <td>{{ $item->amount }}</td>
                                                <td>{{ number_format($item->subtotal) }} ₭</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr class="bg-light">
                                        <td colspan="4"></td>
                                        <td>ລວມຈຳນວນ:</td>
                                        <td class="text-sm">{{ $this->orderdetail_sum_amount }} ລາຍການ
                                    </tr>
                                    <tr class="bg-light">
                                        <td colspan="4"></td>
                                        <td>ລວມເປັນເງິນ:</td>
                                        <td class="text-sm">{{ number_format($this->orderdetail_sum_subtotal) }} ₭
                                    </tr>
                                    <tr class="bg-light">
                                        <td colspan="4"></td>
                                        <td>ທຸລະກຳ:</td>
                                        @if ($this->payment == 1)
                                            <td class="text-danger"><i class="fas fa-hand-holding-usd"></i> ບໍ່ທັນຊຳລະ
                                            </td>
                                        @elseif($this->payment == 2)
                                            <td class="text-success"><i class="fas fa-check-circle"></i> ຊຳລະເເລ້ວ</td>
                                        @endif
                                    </tr>
                                    <tr class="bg-light">
                                        <td colspan="4"></td>
                                        <td>ປະເພດ:</td>
                                        @if ($this->payment_type == 1)
                                            <td class="text-danger"><i class="fas fa-hand-holding-usd"></i> ເງິນສົດ
                                            </td>
                                        @elseif($this->payment_type == 2)
                                            <td class="text-success"><i class="fas fa-check-circle"></i> ເງິນໂອນ</td>
                                        @endif
                                    </tr>
                                    <tr class="bg-light">
                                        <td colspan="4"></td>
                                        <td>ສະຖານະ:</td>
                                        @if ($this->status == 1)
                                            <td class="text-warning"><i class="fas fa-search"></i> ລໍຖ້າກວດສອບ</td>
                                        @elseif($this->status == 2)
                                            <td class="text-success"><i class="fas fa-check-circle"></i> ນຳເຂົ້າສຳເລັດ
                                            </td>
                                        @elseif($this->status == 3)
                                            <td class="text-danger"><i class="fas fa-times-circle"></i> ຖືກຍົກເລີກ</td>
                                        @endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    {{-- <div class="row">
                        <div class="col-5">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th style="width:50%">ລວມຈຳນວນ:</th>
                                        <td class="text-sm">{{ $this->orderdetail_sum_amount }} ລາຍການ
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>ລວມເປັນເງິນ:</th>
                                        <td class="text-sm">{{ number_format($this->orderdetail_sum_subtotal) }} ₭
                                    </tr>
                                    <tr>
                                        <th>ຊຳລະເງິນ:</th>
                                        @if ($this->payment == 1)
                                            <td class="text-danger"><i class="fas fa-hand-holding-usd"></i> ບໍ່ທັນຊຳລະ
                                            </td>
                                        @elseif($this->payment == 2)
                                            <td class="text-success"><i class="fas fa-check-circle"></i> ຊຳລະເເລ້ວ</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>ສະຖານະ:</th>
                                        @if ($this->status == 1)
                                            <td class="text-warning"><i class="fas fa-search"></i> ລໍຖ້າກວດສອບ</td>
                                        @elseif($this->status == 2)
                                            <td class="text-success"><i class="fas fa-check-circle"></i> ນຳເຂົ້າສຳເລັດ
                                            </td>
                                        @elseif($this->status == 3)
                                            <td class="text-danger"><i class="fas fa-times-circle"></i> ຖືກຍົກເລີກ</td>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.invoice -->
