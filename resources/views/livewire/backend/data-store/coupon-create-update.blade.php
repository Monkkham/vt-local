            <!--Foram add new-->
            <div class="col-md-4">
                <div class="card card-default">
                    <div class="card-header bg-green">
                        <label>ເພີ່ມໃຫມ່ / ແກ້ໄຂ</label>
                    </div>
                    <form>
                        <div class="card-body">
                            <input type="hidden" wire:model="ID" value="{{ $ID }}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ເລືອກປະເພດ</label>
                                        <select wire:model="type" class="form-control">
                                            <option value="" selected>ເລືອກ</option>
                                            {{-- @foreach ($customer_type as $item) --}}
                                            <option value="percent">percent</option>
                                            <option value="fixed">fixed</option>
                                            {{-- @endforeach --}}
                                        </select>
                                        @error('type')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ເປີເຊັນສ່ວນຫລຸດ</label>
                                        <input wire:model="value" type="text"
                                            class="form-control @error('value') is-invalid @enderror"
                                            placeholder="ປ້ອນຂໍ້ມູນ">
                                        @error('value')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ເປັນເງິນ</label>
                                        <input wire:model="money" type="text"
                                            class="form-control @error('money') is-invalid @enderror"
                                            placeholder="ປ້ອນຂໍ້ມູນ">
                                        @error('money')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ວັນຫມົດອາຍຸ</label>
                                        <input wire:model="expire_date" type="date"
                                            class="form-control @error('expire_date') is-invalid @enderror"
                                            placeholder="ປ້ອນຂໍ້ມູນ">
                                        @error('expire_date')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-between md-2">
                                {{-- @foreach ($rolepermissions as $items)
                            @if ($items->permissionname->name == 'action_customer_type') --}}
                                <button type="button" wire:click="resetform" class="btn btn-warning">ຣີເຊັດ</button>
                                <button type="button" wire:click="store" class="btn btn-success">ບັນທຶກ</button>
                                {{-- @endif
                              @endforeach --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @push('scripts')
                <script>
                    window.addEventListener('show-modal-delete', event => {
                        $('#modal-delete').modal('show');
                    })
                    window.addEventListener('hide-modal-delete', event => {
                        $('#modal-delete').modal('hide');
                    })
                </script>
            @endpush
