<div>
 <div class="right_col" role="main">
    <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h6><i class="fa fa-building"></i> ກ່ຽວກັບຮ້ານຄ້າ</h6>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">ຫນ້າຫຼັກ</a></li>
                          <li class="breadcrumb-item active">ກ່ຽວກັບຮ້ານຄ້າ</li>
                        </ol>
                      </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" style="background: linear-gradient(90deg, rgb(237, 38, 244) 20%, rgb(240, 32, 32) 52%, rgb(68, 41, 222) 84%);">
                                <h5 style="color:#fff"><b><i class="fa fa-building"></i> ຂໍ້ມູນກ່ຽວກັບຮ້ານຄ້າ</b></h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="control-user">
                                            {{-- @if($new_img)
                                                @if ($img)
                                                    <div class="user-profile" style="border: 5px solid green;">
                                                        @php
                                                            try {
                                                                $url = $img->temporaryUrl();
                                                                $status = true;
                                                            } catch (RuntimeException $exception) {
                                                                $status = false;
                                                            }
                                                        @endphp
                                                        @if ($status)
                                                            <img src="{{ $url }}" alt="logo">
                                                        @endif
                                                    </div>
                                                    <i class="fas fa-check-circle" style="color:green;font-size:20px;padding:10px;"></i>
                                                @else
                                                    <div class="user-profile" style="border: 5px solid green;">
                                                            <img src="{{asset($new_img)}}" alt="logo">
                                                    </div>
                                                    <i class="fas fa-check-circle" style="color:green;font-size:20px;padding:10px;"></i>
                                                @endif
                                            @else
                                                <div class="user-profile" style="border: 5px solid #000">
                                                    <img src="{{ asset('image/logo/lao_youth.png') }}" alt="" width="100px;"
                                                        height="100px;">
                                                </div>
                                            @endif
                                            <label>{{ __('lang.logo') }}</label> --}}
                                            {{-- <div class="col-md-4">
                                                <div class="form-group">
                                                    <input wire:model="img" type="file" placeholder="{{ __('lang.logo') }}"
                                                        class="form-control @error('img') is-invalid @enderror">
                                                    @error('img')
                                                        <span style="color: red" class="error">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">ຊື່ອົງກອນ</label>
                                            <input type="text" wire:model="name" class="form-control" placeholder="ຊື່ອົງກອນ">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">ຂໍ້ມູນຕິດຕໍ່</label>
                                            <input type="text" wire:model="phone" class="form-control" placeholder="ຂໍ້ມູນຕິດຕໍ່">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">ອີເມວ</label>
                                            <input type="text" wire:model="email" class="form-control" placeholder="ອີເມວ">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="detail">ທີ່ຢູ່ສຳນັກງານໃຫຍ່</label>
                                            <div wire:ignore>
                                                <textarea class="form-control" id="address" wire:model="address">{{$address}}</textarea>
                                            </div>
                                            @error('address')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="detail">ຄຳອະທິບາຍ</label>
                                            <div wire:ignore>
                                                <textarea class="form-control" id="note" wire:model="note">{{$note}}</textarea>
                                            </div>
                                            @error('note')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="detail">ລະບຽບຫຼັກການ</label>
                                            <div wire:ignore>
                                                <textarea class="form-control" id="role" wire:model="role">{{$role}}</textarea>
                                            </div>
                                            @error('role')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12" wire:ignore>
                                        <div id="map-update-s" style="height:250px; width: 100%;" class="my-3"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>ສະເເດງແຜນທີ່</label>
                                            <button type="button" class="btn btn-outline-primary form-control"><a href="#" onclick="getLocations()"><i class="fa fa-map-marker"></i> ສະເເດງແຜນທີ່ <i class="icon-long-arrow-right"></i></a></button>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>lat</label>
                                            <input wire:model="lats" placeholder="lat" type="text"
                                                class="form-control @error('lats') is-invalid @enderror" id="lats" readonly>
                                            @error('lats')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>long</label>
                                            <input wire:model="longs" placeholder="long" type="text"
                                                class="form-control @error('longs') is-invalid @enderror" id="longs" readonly>
                                            @error('longs')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="address">{{__('lang.address')}}{{__('lang.languages')}}{{__('lang.lao')}}</label>
                                            <div wire:ignore>
                                                <textarea class="form-control" id="address_la" wire:model="address_la">{{$address_la}}</textarea>
                                            </div>
                                            @error('address_la')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div> --}}
                                    {{-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="address">{{__('lang.address')}}{{__('lang.languages')}}{{__('lang.en')}}</label>
                                            <div wire:ignore>
                                                <textarea class="form-control" id="address_en" wire:model="address_en">{{$address_en}}</textarea>
                                            </div>
                                            @error('address_en')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="card-footer">
                                {{-- @foreach ($rolepermissions as $items)
                                @if ($items->permissionname->name == 'action_about') --}}
                                <button wire:click="update" class="btn btn-success"><i class="fa fa-edit"></i> ບັນທຶກແກ້ໄຂ</button>
                                <button class="btn btn-warning float-right"><i class="fa fa-trash"></i> ລ້າງຂໍ້ມູນ</button>
                               {{-- @endif
                                @endforeach --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </section>
        </div>
    </div>
    @include('livewire.backend.store.script')
    