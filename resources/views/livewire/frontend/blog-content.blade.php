<main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
                <li class="breadcrumb-item active" aria-current="page">ປະກາດຂ່າວ</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    @foreach ($post_public as $item)
                        <article class="entry entry-list">
                            <div class="row align-items-center">
                                <div class="col-md-5">
                                    <figure class="entry-media">
                                        <a href="single.html">
                                            <img src="{{ asset($item->image) }}" alt="image desc">
                                        </a>
                                    </figure>
                                    <!-- End .entry-media -->
                                </div>
                                <!-- End .col-md-5 -->

                                <div class="col-md-7">
                                    <div class="entry-body">
                                        <div class="entry-meta">
                                            <span class="entry-author">
                                                <a href="#">ວັນທີ່</a>
                                            </span>
                                            <span class="meta-separator">|</span>
                                            <a href="#">{{ $item->created_at }}</a>
                                            <span class="meta-separator">|</span>
                                            <a href="#">2 Comments</a>
                                        </div>
                                        <!-- End .entry-meta -->
                                        <div class="entry-content">
                                            <p>{!! $item->note !!}</p>
                                            <a href="#" class="read-more">ປະກາດຂ່າວປະຈຳອາທິດ</a>
                                        </div>
                                        <!-- End .entry-content -->
                                    </div>
                                    <!-- End .entry-body -->
                                </div>
                                <!-- End .col-md-7 -->
                            </div>
                            <!-- End .row -->
                        </article>
                    @endforeach
                    <!-- End .entry -->
                </div>
                <!-- End .col-lg-9 -->
            </div>
            <!-- End .row -->
        </div>
        <!-- End .container -->
    </div>
</main><!-- End .main -->
