<main wire:ignore.self class="main">
    <div class="intro-section pt-3 pb-3 mb-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="intro-slider-container slider-container-ratio mb-2 mb-lg-0">
                        <div class="intro-slider owl-carousel owl-simple owl-dark owl-nav-inside" data-toggle="owl"
                            data-owl-options='{
                                "nav": false, 
                                "dots": true,
                                "responsive": {
                                    "768": {
                                        "nav": true,
                                        "dots": false
                                    }
                                }
                            }'>
                            @foreach ($slider as $item)
                                <div class="intro-slide"
                                    style="background-image: url({{ asset($item->image) }});">
                                    <div class="intro-content pl-5">
                                        <h1 class="intro-title text-white">
                                            {{ $item->name }}
                                        </h1><!-- End .intro-title -->
                                        <a href="{{ route('frontend.shop') }}" class="btn btn-danger btn-round">
                                            <span>ໄປເບິ່ງສິນຄ້າ</span>
                                            <i class="icon-long-arrow-right"></i>
                                        </a>
                                    </div><!-- End .intro-content -->
                                </div><!-- End .intro-slide -->
                            @endforeach
                        </div><!-- End .intro-slider owl-carousel owl-simple -->

                        <span class="slider-loader"></span><!-- End .slider-loader -->
                    </div><!-- End .intro-slider-container -->
                </div><!-- End .col-lg-8 -->

                <div class="col-lg-4">
                    <div class="intro-banners">
                        <div class="banner mb-lg-1 mb-xl-2">
                            <a href="#">
                                <img style="height: 130px; width:100%" src="{{ asset('slide/p1.jpg') }}"
                                    alt="Banner">
                            </a>

                            <div class="banner-content">

                                <h3 class="banner-title text-white"><a href="#">ສຳຫຼັບທ່ານຊາຍ</a></h3>
                                <!-- End .banner-title text-white -->
                                <a href="{{ route('frontend.shop') }}" class="banner-link">ເລືອກຊື້ເລີຍ<i
                                        class="icon-long-arrow-right"></i></a>
                            </div><!-- End .banner-content -->
                        </div><!-- End .banner -->

                        <div class="banner mb-lg-1 mb-xl-2">
                            <a href="#">
                                <img style="height: 130px; width:100%" src="{{ asset('slide/p3.jpg') }}"
                                    alt="Banner">
                            </a>

                            <div class="banner-content">

                                <h3 class="banner-title text-white"><a href="#">ສຳຫຼັບທ່ານຍິງ </a></h3>
                                <!-- End .banner-title -->
                                <a href="{{ route('frontend.shop') }}" class="banner-link">ເລືອກຊື້ເລີຍ<i
                                        class="icon-long-arrow-right"></i></a>
                            </div><!-- End .banner-content -->
                        </div><!-- End .banner -->

                        <div class="banner mb-0">
                            <a href="#">
                                <img style="height: 130px; width:100%" src="{{ asset('slide/p2.jpg') }}"
                                    alt="Banner">
                            </a>

                            <div class="banner-content">
                                <h4 class="banner-subtitle d-lg-none d-xl-block"><a href="#">Featured</a></h4>
                                <!-- End .banner-subtitle -->
                                <h3 class="banner-title text-white"><a href="#">ທົ່ວໄປ <span></span></a></h3>
                                <!-- End .banner-title -->
                                <a href="{{ route('frontend.shop') }}" class="banner-link">ເລືອກຊື້ເລີຍ<i
                                        class="icon-long-arrow-right"></i></a>
                            </div><!-- End .banner-content -->
                        </div><!-- End .banner -->
                    </div><!-- End .intro-banners -->
                </div><!-- End .col-lg-4 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .intro-section -->

    <div class="mb-4"></div><!-- End .mb-2 -->



    <div class="mb-2"></div><!-- End .mb-2 -->


    <div class="mb-3"></div><!-- End .mb-3 -->

    <div class="bg-light pt-3 pb-5">
        <div class="container">
            <div class="heading heading-center mb-6">
                <h2 class="title"><i class="fas fa-tshirt"></i> ສິນຄ້າທັງຫມົດ</h2>
                <!-- End .title -->
            </div>
            <div wire:ignore class="tab-content tab-content-carousel">
                <div class="tab-pane p-0 fade show active" id="trending-women-tab" role="tabpanel"
                    aria-labelledby="trending-women-link">
                    <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl"
                        data-owl-options='{
                            "nav": false, 
                            "dots": true,
                            "margin": 20,
                            "loop": false,
                            "responsive": {
                                "0": {
                                    "items":2
                                },
                                "480": {
                                    "items":2
                                },
                                "768": {
                                    "items":3
                                },
                                "992": {
                                    "items":4
                                },
                                "1200": {
                                    "items":4,
                                    "dots": false
                                }
                            }
                        }'>
                        @foreach ($product_all as $item)
                            <div class="product product-7">
                                <figure class="product-media">
                                    <a href="javascript:void(0)" wire:click="ViewProductDetail({{ $item->id }})">
                                        <img src="{{ asset($item->image) }}"
                                            style="width: 100%; height: 180px;">
                                    </a>

                                    <div class="product-action-vertical">
                                        <button type="button"
                                            wire:click="addToWishlist({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                            class="btn-product-icon btn-wishlist btn-expandable"><span>ເພີ່ມລາຍການທີ່ມັກ</span></button>

                                    </div><!-- End .product-action-vertical -->
                                    @if ($item->qty > 0)
                                        <div class="product-action">
                                            <button type="button"
                                                wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                                class="btn-product btn-cart"><span>ເພີ່ມໃສ່ກະຕ່າ</span></button>
                                        </div><!-- End .product-action -->
                                    @endif
                                </figure><!-- End .product-media -->

                                <div class="product-body">
                                    <div class="product-cat">
                                        @if (!empty($item->product_type))
                                            <a href="#">ຫມວດຫມູ່:
                                                {{ $item->product_type->name }}
                                            @else
                                        @endif
                                        </a>
                                    </div><!-- End .product-cat -->
                                    <h3 class="product-title"><a href="product.html">{{ $item->name }}</a>
                                        <p>
                                            @if ($item->qty > 0)
                                                <p class="text-success">In Stock</p>
                                            @endif
                                        </p>
                                    </h3><!-- End .product-title -->
                                    <div class="product-price">
                                        <div class="product">
                                            <span class="new-price">ລາຄາ: {{ number_format($item->sell_price) }}
                                                ₭</span>
                                        </div>
                                        <s>
                                            <span class="old-price">ປົກກະຕິ:
                                                {{ number_format($item->promotion_price) }} ₭</span>
                                        </s>
                                    </div><!-- End .product-price -->

                                    <div class="ratings-container">
                                    </div>
                                    <div class="product">
                                        @if ($item->qty > 0)
                                        @else
                                            <p class="text-danger text-center"><i class="fas fa-box-open"></i>
                                                ສິນຄ້າຫມົດ!</p>
                                        @endif
                                    </div><!-- End .product-action -->
                                    {{-- @endif --}}
                                </div><!-- End .product-body -->
                            </div><!-- End .product -->
                        @endforeach
                    </div>
                </div>
            </div>
        </div><!-- End .container -->
    </div><!-- End .bg-light pt-5 pb-5 -->

    <div class="mb-3"></div><!-- End .mb-3 -->

    <div wire:ignore class="container electronics">
        <div class="heading heading-flex heading-border mb-3">
            <div class="heading-left">
                <h2 class="title"><i class="fas fa-tshirt"></i> ປະເພດເຄື່ອງນຸ່ງຮົ່ມ</h2><!-- End .title -->
            </div><!-- End .heading-left -->

            <div class="heading-right">
                <ul class="nav nav-pills nav-border-anim justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="elec-new-link" data-toggle="tab" href="#elec-new-tab"
                            role="tab" aria-controls="elec-new-tab" aria-selected="true">ໃຫມ່</a>
                    </li>
                </ul>
            </div><!-- End .heading-right -->
        </div><!-- End .heading -->

        <div wire:ignore class="tab-content tab-content-carousel">
            <div class="tab-pane p-0 fade show active" id="trending-women-tab" role="tabpanel"
                aria-labelledby="trending-women-link">
                <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl"
                    data-owl-options='{
                        "nav": false, 
                        "dots": true,
                        "margin": 20,
                        "loop": false,
                        "responsive": {
                            "0": {
                                "items":2
                            },
                            "480": {
                                "items":2
                            },
                            "768": {
                                "items":3
                            },
                            "992": {
                                "items":4
                            },
                            "1200": {
                                "items":4,
                                "dots": false
                            }
                        }
                    }'>
                    @foreach ($product_type_food as $item)
                        <div class="product product-7">
                            <figure class="product-media">
                                <a href="javascript:void(0)" wire:click="ViewProductDetail({{ $item->id }})">
                                    <img src="{{ asset($item->image) }}"
                                        style="width: 100%; height: 180px;">
                                </a>

                                <div class="product-action-vertical">
                                    <button type="button"
                                        wire:click="addToWishlist({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                        class="btn-product-icon btn-wishlist btn-expandable"><span>ເພີ່ມລາຍການທີ່ມັກ</span></button>

                                </div><!-- End .product-action-vertical -->
                                @if ($item->qty > 0)
                                    <div class="product-action">
                                        <button type="button"
                                            wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                            class="btn-product btn-cart"><span>ເພີ່ມໃສ່ກະຕ່າ</span></button>
                                    </div><!-- End .product-action -->
                                @endif
                            </figure><!-- End .product-media -->

                            <div class="product-body">
                                <div class="product-cat">
                                    @if (!empty($item->product_type))
                                        <a href="#">ຫມວດຫມູ່:
                                            {{ $item->product_type->name }}
                                        @else
                                    @endif
                                </div><!-- End .product-cat -->
                                <h3 class="product-title"><a href="product.html">{{ $item->name }}</a>
                                    <p>
                                        @if ($item->qty > 0)
                                            <p class="text-success">In Stock</p>
                                        @endif
                                    </p>
                                </h3><!-- End .product-title -->
                                <div class="product-price">
                                    <div class="product">
                                        <span class="new-price">ລາຄາ: {{ number_format($item->sell_price) }}
                                            ₭</span>
                                    </div>
                                    <s>
                                        <span class="old-price">ປົກກະຕິ:
                                            {{ number_format($item->promotion_price) }} ₭</span>
                                    </s>
                                </div><!-- End .product-price -->

                                <div class="ratings-container">
                                </div>
                                <div class="product">
                                    @if ($item->qty > 0)
                                    @else
                                        <p class="text-danger text-center"><i class="fas fa-box-open"></i>
                                            ສິນຄ້າຫມົດ!</p>
                                    @endif
                                </div><!-- End .product-action -->
                                {{-- @endif --}}
                            </div><!-- End .product-body -->
                        </div><!-- End .product -->
                    @endforeach
                </div>
            </div>
        </div>
    </div><!-- End .container -->

    <div class="mb-3"></div><!-- End .mb-3 -->


    <div class="mb-1"></div><!-- End .mb-1 -->

    <div wire:ignore class="container furniture">
        <div class="heading heading-flex heading-border mb-3">
            <div class="heading-left">
                <h2 class="title"><i class="fas fa-tshirt"></i> ປະເພດເຄື່ອງປະດັບ</h2><!-- End .title -->
            </div><!-- End .heading-left -->

            <div class="heading-right">
                <ul class="nav nav-pills nav-border-anim justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="furn-new-link" data-toggle="tab" href="#furn-new-tab"
                            role="tab" aria-controls="furn-new-tab" aria-selected="true">ໃຫມ່</a>
                    </li>
                </ul>
            </div><!-- End .heading-right -->
        </div><!-- End .heading -->

        <div wire:ignore class="tab-content tab-content-carousel">
            <div class="tab-pane p-0 fade show active" id="trending-women-tab" role="tabpanel"
                aria-labelledby="trending-women-link">
                <div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl"
                    data-owl-options='{
                        "nav": false, 
                        "dots": true,
                        "margin": 20,
                        "loop": false,
                        "responsive": {
                            "0": {
                                "items":2
                            },
                            "480": {
                                "items":2
                            },
                            "768": {
                                "items":3
                            },
                            "992": {
                                "items":4
                            },
                            "1200": {
                                "items":4,
                                "dots": false
                            }
                        }
                    }'>
                    @foreach ($product_type_use as $item)
                        <div class="product product-7">
                            <figure class="product-media">
                                <a href="javascript:void(0)" wire:click="ViewProductDetail({{ $item->id }})">
                                    <img src="{{ asset($item->image) }}"
                                        style="width: 100%; height: 180px;">
                                </a>

                                <div class="product-action-vertical">
                                    <button type="button"
                                        wire:click="addToWishlist({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                        class="btn-product-icon btn-wishlist btn-expandable"><span>ເພີ່ມລາຍການທີ່ມັກ</span></button>

                                </div><!-- End .product-action-vertical -->
                                @if ($item->qty > 0)
                                    <div class="product-action">
                                        <button type="button"
                                            wire:click="addtoCart({{ $item->id }},'{{ $item->name }}',{{ $item->sell_price }})"
                                            class="btn-product btn-cart"><span>ເພີ່ມໃສ່ກະຕ່າ</span></button>
                                    </div><!-- End .product-action -->
                                @endif
                            </figure><!-- End .product-media -->

                            <div class="product-body">
                                <div class="product-cat">
                                    @if (!empty($item->product_type))
                                        <a href="#">ຫມວດຫມູ່:
                                            {{ $item->product_type->name }}
                                        @else
                                    @endif
                                    </a>
                                </div><!-- End .product-cat -->
                                <h3 class="product-title"><a href="product.html">{{ $item->name }}</a>
                                    <p>
                                        @if ($item->qty > 0)
                                            <p class="text-success">In Stock</p>
                                        @endif
                                    </p>
                                </h3><!-- End .product-title -->
                                <div class="product-price">
                                    <div class="product">
                                        <span class="new-price">ລາຄາ: {{ number_format($item->sell_price) }}
                                            ₭</span>
                                    </div>
                                    <s>
                                        <span class="old-price">ປົກກະຕິ:
                                            {{ number_format($item->promotion_price) }} ₭</span>
                                    </s>
                                </div><!-- End .product-price -->

                                <div class="ratings-container">
                                </div>
                                <div class="product">
                                    @if ($item->qty > 0)
                                    @else
                                        <p class="text-danger text-center"><i class="fas fa-box-open"></i>
                                            ສິນຄ້າຫມົດ!</p>
                                    @endif
                                </div><!-- End .product-action -->
                                {{-- @endif --}}
                            </div><!-- End .product-body -->
                        </div><!-- End .product -->
                    @endforeach
                </div>
            </div>
        </div>
    </div><!-- End .container -->

    <div class="mb-3"></div><!-- End .mb-3 -->

    <div class="mb-3"></div><!-- End .mb-3 -->

    <div class="cta cta-horizontal cta-horizontal-box bg-danger">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-2xl-5col">
                    <h3 class="cta-title text-white">ຕິດຕໍ່ຮ້ານຄ້າ</h3><!-- End .cta-title -->
                    <p class="cta-desc text-white">ສົ່ງອີເມວເພື່ອຮ້ອງຂໍລະຫັດສ່ວນຫຼຸດສິນຄ້າ</p>
                    <!-- End .cta-desc -->
                </div><!-- End .col-lg-5 -->

                <div class="col-3xl-5col">
                    <form action="#">
                        <div class="input-group">
                            <input type="email" class="form-control form-control-white"
                                placeholder="Enter your Email Address" aria-label="Email Adress" required>
                            <div class="input-group-append">
                                <button class="btn btn-outline-white-2" type="submit"><span>Subscribe</span><i
                                        class="icon-long-arrow-right"></i></button>
                            </div><!-- .End .input-group-append -->
                        </div><!-- .End .input-group -->
                    </form>
                </div><!-- End .col-lg-7 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .cta -->

    <div wire:ignore class="blog-posts bg-light pt-4 pb-7">
        <div class="container">
            <h2 class="title"><i class="fas fa-blog"></i> ອັບເດດຂ່າວກ່ຽວກັບຮ້ານຄ້າ</h2>
            <!-- End .title-lg text-center -->
            <div class="owl-carousel owl-simple" data-toggle="owl"
                data-owl-options='{
                    "nav": false, 
                    "dots": true,
                    "items": 3,
                    "margin": 20,
                    "loop": false,
                    "responsive": {
                        "0": {
                            "items":1
                        },
                        "600": {
                            "items":2
                        },
                        "992": {
                            "items":3
                        },
                        "1280": {
                            "items":4,
                            "nav": true, 
                            "dots": false
                        }
                    }
                }'>
                @foreach ($post_public as $item)
                    <article class="entry">
                        <figure class="entry-media">
                            <a href="#">
                                <img src="{{ asset($item->image) }}" style="width: 100%; height: 200px;"
                                    alt="image desc">
                            </a>
                        </figure><!-- End .entry-media -->

                        <div class="entry-body">
                            <div class="entry-meta">
                                <a href="#">ວັນທີ່: {{ $item->created_at }}</a>
                            </div><!-- End .entry-meta -->

                            <div class="entry-content">
                                <p>{!! $item->note !!}</p>
                            </div><!-- End .entry-content -->
                        </div><!-- End .entry-body -->
                    </article><!-- End .entry -->
                @endforeach

            </div><!-- End .owl-carousel -->
        </div><!-- End .container -->
    </div><!-- End .blog-posts -->
</main><!-- End .main -->
