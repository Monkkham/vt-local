<div class="page-header text-center" style="background-image: linear-gradient(#246dea, #11dfcb">
    <div class="container">
        <h3 class="page-title text-white"><i class="fas fa-tags"></i> ແຈກລະຫັດສ່ວນຫຼຸດວັນເທດສະການ</h3>
    </div><!-- End .container -->
</div><!-- End .page-header -->
<nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html">ຫນ້າຫຼັກ</a></li>
            <li class="breadcrumb-item active" aria-current="page">ໂປຣໂມຊັ່ນ</li>
        </ol>
    </div><!-- End .container -->
</nav><!-- End .breadcrumb-nav -->
<section id="labels">
    <div class="container">
      <div class="row">
          @foreach ($promotion as $item)
          <div class="col-sm-6 col-md-3 mt-2">
            <div class="dl">
              <div class="brand">
                  <h6>ວັນຫມົດກຳນົດ: {{ date('d/m/Y', strtotime($item->expire_date)) }}</h6>
              </div>
              <div class="discount alizarin">{{ $item->value }}%
                  <div class="type">ດ່ວນ!</div>
              </div>
              <div class="descr">
                  <strong>ຊື້ສິນຄ້າໃນລາຄາ: {{ number_format($item->money) }} ກີບຂື້ນໄປ</strong> 
              </div>
              <div class="coupon midnight-blue">
                  <a data-toggle="collapse" href="#code-1" class="open-code">ຮັບລະຫັດເລີຍ</a>
                  <div id="code-1" class="collapse code">{{ $item->code }}</div>
              </div>
            </div>
          </div>
          @endforeach
      </div>
    </div>
  </section>
  <style>
    section{padding:5% 0}
.alizarin{background:#e74c3c}
.amethyst{background:#9b59b6}
.emerald{background:#2ecc71}
.midnight-blue{background:#2c3e50}
.peter-river{background:#3498db}
.dl{background:#f0f0f0;padding:30px 0;border-radius:20px;position:relative}
.dl:before{content:" ";height:20px;width:20px;background:#ddd;border-radius:20px;position:absolute;left:50%;top:20px;margin-left:-10px}
.dl .brand{text-transform:uppercase;letter-spacing:3px;padding:10px 15px;margin-top:10px;text-align:center;min-height:100px}
.dl .discount{min-height:50px;position:relative;font-size:80px;line-height:80px;text-align:center;font-weight:700;padding:20px 15px 0;color:#f1c40f}
.dl .discount:after{content:" ";border-right:20px solid transparent;border-left:20px solid transparent;position:absolute;bottom:-20px;left:20%}
.dl .discount.alizarin:after{border-top:20px solid #e74c3c}
.dl .discount.peter-river:after{border-top:20px solid #3498db}
.dl .discount.emerald:after{border-top:20px solid #2ecc71}
.dl .discount.amethyst:after{border-top:20px solid #9b59b6}
.dl .discount .type{font-size:20px;letter-spacing:1px;text-transform:uppercase;margin-top:-30px}
.dl .descr{color:#999;margin-top:10px;padding:20px 15px}
.dl .ends{padding:0 15px;color:#f1c40f;margin-bottom:10px}
.dl .coupon{min-height:50px;text-align:center;text-transform:uppercase;font-weight:700;font-size:18px;padding:20px 15px}
.dl .coupon a.open-code{color:#16a085}
.dl .coupon .code{letter-spacing:1px;border-radius:4px;margin-top:10px;padding:10px 15px;color:#f1c40f;background:#f0f0f0}
  </style>