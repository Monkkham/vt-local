<div class="header-left">
    <div class="dropdown category-dropdown">
        <a href="#" class="dropdown-toggle bg-success" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static" title="ຫມວດຫມູ່ສິນຄ້າ">
            ຫມວດຫມູ່ສິນຄ້າ
        </a>
        <div class="dropdown-menu">
            <nav class="side-nav">
                <ul class="menu-vertical sf-arrows">
                    @foreach ($categorys as $item)
                    <li><a href="{{ route('frontend.category_search') }}"><i class="fas fa-football-ball"></i> {{ $item->name }}</a></li>
                    @endforeach
                </ul><!-- End .menu-vertical -->
            </nav><!-- End .side-nav -->
        </div><!-- End .dropdown-menu -->
    </div><!-- End .category-dropdown -->
</div><!-- End .header-left -->