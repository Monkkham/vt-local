@if (!empty(Cart::instance('wishlist')->count()))
    <span class="wishlist-count">{{ Cart::instance('wishlist')->count() }}</span>
@endif
